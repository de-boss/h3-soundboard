from glob import glob
from pathlib import Path
import re

dbutton = "https://img.icons8.com/ultraviolet/24/000000/download.png"
folder = glob('~/h3-soundboard/H3 Drops/*')

d = {}

for i in folder:
	name = Path(i).stem
	d[name] = [Path(x).stem for x in glob(f'{i}/*')]


sb = {}

for x in d.keys():
	links = [f'https://gitlab.com/de-boss/h3-soundboard/-/raw/2e80a4c455ebe0025a89e91afd222dc11602dd42/H3%20Drops/{x}/{y}.mp3' for y in d[x]]
	links = [url.replace(' ', '%20') for url in links]
	links = [url.replace("'", "\'") for url in links]
	sb[x] = links


for q, target in enumerate(sorted(list(sb.keys()))):

	target_html = target.replace(' ', '%20')

	print(f"""
		<div class="tab">
		<input type="checkbox" id="chck{q}">
		<label class="tab-label" for="chck{q}">{target}</label>
		<div class="tab-content">
		  """)

	for num, bite in enumerate(sb[target]):
		file_name = re.split(f'\\b/{target_html}/\\b', bite)[-1]
		file_name = file_name.replace('%20', ' ')[:-4]

		print(f"""
			<div class="list__item" data-key="{num}_{file_name}">
				  <div class="list__item-key">
					<a href={bite}>
		<img src={dbutton} alt="download" width="20" height="20">
		</a></div>
				  <div class="list__item-quote">"{file_name}"</div>
				</div>""")

		print(f"""<audio data-key="{num}_{file_name}" src={bite}></audio>
			""")

	print("""</div>
		</div>""")
